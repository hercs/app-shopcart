Desenvolvimento de app flask

## Getting Started

No desenvolvimento desse projeto procurei adicionar alguns itens que acredito que seriam interessantes para este desafio, como um ORM de banco de dados, documentação da API, JWT, logging. Muita coisa poderia ser evoluída e adicionada, mas acredito que consegui atender aos requisitos do desafio.
Para executar o projeto é necessário seguir o passos abaixo:

### Dependências

```
git
python3.5 ou superior
mysql
docker
virtualenv
pip

```

### Installing

Instalar os pré-requisitos de acordo com a plataforma

No ubuntu é necessário instalar os pacotes:

```
sudo apt-get install python3-dev libmysqlclient-dev
```

## Run

Para rodar o projeto é necessário que o banco de dados (mysql) esteja rodando e o database "shopcart_db" criado.

Se tiver o banco de dados rodando, apenas crie o database com o seguinte comando:

```
CREATE DATABASE shopcart_db CHARACTER SET utf8 COLLATE utf8_general_ci;
```

Depois importe a carga inicial do projeto através do script 'shopcart.sql'
Execute o comando:

```
mysql -u USUARIO -p shopcart_db < shopcart.sql
```

Clonar o projeto:

```
git clone git@bitbucket.org:hercs/app-shopcart.git
```

Após clonar o projeto criar uma variável de ambiente informando onde o banco de dados está rodando:

```
export DATABASE_URI='mysql://USURIO:SENHA@HOST:PORT/shopcart_db'
```

Em seguida, na raíz do projeto, crie um ambiente virtual com o comando:

```
virtualenv -p python3 venv
```

Ative o virtualenv

```
source venv/bin/activate
```

Instale as dependências

```
pip install -r requirements.txt
```

Rode o projeto com o comando:

```
python run.py
```

Acesse o endereço: http://localhost:5000/apidocs/

Se quiser rodar em uma porta diferente defina a variável de ambiente:
```
export PORT=8080
```
## Testando a aplicação

O banco de dados já está populado com alguns registros, sendo possível realizar alguns testes.

No endpoint de produtos (http://localhost:5000/apidocs/#!/Product) click em **GET** /api/products para ver a lista de produtos sem precisar de autenticação.

http://localhost:5000/apidocs/#!/Product/get_api_products

Para adicionar um item ao carrinho precisa estar autenticado no sistema, sendo necessário realizar login.
Click no endpoint de usuário: http://localhost:5000/apidocs/#!/User

```
http://localhost:5000/apidocs/#!/User/post_api_login
```
No corpo (body) da requisição coloque o json:

```
{
"username":"admin",
"password":"admin"
}
```
No response body será o retirnado o token de acesso, copie o token para realizar os testes que necessitam de autenticação.

```
"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI..."
```

Adicione um item ao carrinho, clicando em http://localhost:5000/apidocs/#!/Shopcart

E depois click em http://localhost:5000/apidocs/#!/Shopcart/post_api_cart

No campo Authorization informe o token gerado no login da seguinte forma:

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```

E no campo Body da requisição coloque o json a seguir e envie:

```
{
"product_id":"1",
"quantity":"1"
}
```

Verifique o carrinho em http://localhost:5000/apidocs/#!/Shopcart/get_api_cart  e informando o token

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```

Adicione outro produto seguindo os mesmo passos:

E no campo Body da requisição coloque o json a seguir e envie:

```
{
"product_id":"2",
"quantity":"3"
}
```
Envie e verifique

Remova um item do carrinho clicando em http://localhost:5000/apidocs/#!/Shopcart/delete_api_cart  e informando o token

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```
E no campo Body da requisição coloque o json a seguir e envie:

```
{
"product_id":"1"
}
```
Envie e verifique

Para finalizar realize o **checkout** do carrinho:

Click em http://localhost:5000/apidocs/#!/Checkout

E depois em http://localhost:5000/apidocs/#!/Checkout/post_api_checkout

Apenas informe o token e envie

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```

Você receberá no response body:

```
{
  "message": "Payment done."
}
```

Agora executando o **GET** do carrinho você receberá uma mensagem de carrinho vazio.


## License

This project is licensed under the MIT License

