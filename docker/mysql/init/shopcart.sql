CREATE DATABASE IF NOT EXISTS `shopcart_db` DEFAULT CHARACTER SET utf8;

USE `shopcart_db`;

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`user`,`role`),
  KEY `role` (`role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`user_id`),
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`role`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `fator`;
CREATE TABLE `fator` (
  `fator_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `fator_id` int(11) NOT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  KEY `fator_id` (`fator_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`fator_id`) REFERENCES `fator` (`fator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `role` VALUES (1,'ADMIN','2017-11-30 01:27:55','2017-11-30 01:27:55');
INSERT INTO `user` VALUES (1,'admin','$pbkdf2-sha256$29000$zZkzJiRkrLUWonROSSnFuA$0cTt.xgVLg8E.fkOvdUg0xEU/7VxuHy66TEGQPuphbU','admin@acme.com','2017-11-30 01:27:28','2017-11-30 01:27:28');
INSERT INTO `user_roles` VALUES (1,1);
INSERT INTO `fator` VALUES (1,'A','Fator A','2017-11-30 02:00:20','2017-11-30 02:00:20'),(2,'B','Fator B','2017-11-30 02:01:34','2017-11-30 02:01:34'),(3,'C','Fator C','2017-11-30 02:01:47','2017-11-30 02:01:47');
INSERT INTO `product` VALUES (1,'SmartTV 40','SmartTV Samsung 40 polegadas','photo.jpg',2800,3,'2017-11-30 02:06:09','2017-11-30 02:06:09'),(2,'Teclado Microsoft','Teclado Microsoft','photo.jpg',211.34,2,'2017-11-30 02:22:28','2017-11-30 02:22:28');