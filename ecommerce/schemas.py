from marshmallow import Schema, fields

class FatorSchema(Schema):
    fator_id = fields.Int()
    name = fields.Str()
    description = fields.Str()

class ProductSchema(Schema):
    product_id = fields.Int()
    name = fields.Str()
    description = fields.Str()
    image = fields.Str()
    price = fields.Str()
    fator = fields.Nested(FatorSchema)
    updated_on = fields.DateTime('%Y-%m-%dT%H:%M:%S+03:00')

class UserSchema(Schema):
    user_id = fields.Int()
    username = fields.Str()
    email = fields.Str()
    updated_on = fields.DateTime('%Y-%m-%dT%H:%M:%S+03:00')

product_schema = ProductSchema()
products_schema = ProductSchema(many=True)
fator_schema = FatorSchema()
fatores_schema = FatorSchema(many=True)
user_schema = UserSchema()
users_schema = UserSchema(many=True)