from passlib.hash import pbkdf2_sha256 as sha256
from datetime import datetime
from ecommerce import db

class User(db.Model):
    user_id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(120), unique = True, nullable = False)
    password = db.Column(db.String(120), nullable = False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    items = db.relationship('Shopcart', backref='user', lazy=True)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def save_user(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_user(self):
        db.session.delete(self)
        db.session.commit()

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password
    
    def __repr__(self):
        return '<User {}>'.format(self.name)

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username = username).first()

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)
    
    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

user_role = db.Table('user_roles',
    db.Column('user', db.Integer, db.ForeignKey('user.user_id'), primary_key=True),
    db.Column('role', db.Integer, db.ForeignKey('role.role_id'), primary_key=True)
)

class Role(db.Model):
    role_id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)
    users = db.relationship('User', secondary=user_role, backref=db.backref('roles', lazy='dynamic'))
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def __init__(self, name):
        self.name = name

class Shopcart(db.Model):
    shopcart_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.product_id'), nullable=False)
    quantity = db.Column(db.Integer)
    checked_out = db.Column(db.Boolean, default=False, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def save_shopcart(self):
        db.session.add(self)
        db.session.commit()

    def remove_item(self):
        db.session.delete(self)
        db.session.commit()

    def __init__(self, user_id, product_id, quantity):
        self.user_id = user_id
        self.product_id = product_id
        self.quantity = quantity
    
    def __repr__(self):
        return '<Shopcart {}>'.format(self.shopcart_id)

    @classmethod
    def find_items_by_user(cls, user_id):
        return cls.query.filter_by(user_id=user_id, checked_out=0).all()

    @classmethod
    def find_item_by_product_id(cls, product_id):
        return cls.query.filter_by(product_id=product_id, checked_out=0).first()

    @staticmethod
    def update_cart_checkout(user_id):
        db.session.query(Shopcart).filter_by(user_id=user_id).update(dict(checked_out=True))
        db.session.commit()

    @staticmethod
    def process_shopcart(list_items):
        count_total_discount = 0
        product_price = 0
        total_items = 0
        for i in list_items:
            if i.product.fator.name == 'A':
                count_total_discount += 1
            elif i.product.fator.name == 'B':
                count_total_discount += 5
            elif i.product.fator.name == 'C':
                count_total_discount += 15
            else:
                continue
            product_price += i.product.price * i.quantity
            count_total_discount *= i.quantity
            total_items += i.quantity
            
        if count_total_discount > 30:
            count_total_discount = 30

        price = product_price * (1 - (count_total_discount * .01))
        total_price = float(format(price, '.2f'))

        return {
            'total_price':total_price,
            'total_items': total_items,
            'items':[{'quantity':i.quantity,'name':i.product.name, 'price':i.product.price} for i in list_items],
        }

class Fator(db.Model):
    fator_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10))
    description = db.Column(db.String(255))
    products = db.relationship('Product', backref='fator', lazy=True)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def save_fator(self):
        db.session.add(self)
        db.session.commit()

    def delete_fator(self):
        db.session.delete(self)
        db.session.commit()

    def __init__(self, name, description):
        self.name = name
        self.description = description

    def __repr__(self):
        return '<Fator {}>'.format(self.name)

class Product(db.Model):
    product_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    description = db.Column(db.String(255))
    image = db.Column(db.String(255))
    price = db.Column(db.Float)
    fator_id = db.Column(db.Integer, db.ForeignKey('fator.fator_id'), nullable=False)
    items = db.relationship('Shopcart', backref='product', lazy=True)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def save_product(self):
        db.session.add(self)
        db.session.commit()

    def delete_product(self):
        db.session.delete(self)
        db.session.commit()

    def __init__(self, name, description, image, price, fator):
        self.name = name
        self.description = description
        self.image = image
        self.price = price
        self.fator = fator

    def __repr__(self):
        return '<Product {}>'.format(self.name)